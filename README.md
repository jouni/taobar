# taobar

taobar is a status bar for dwl based on [dtao](https://github.com/djpohly/dtao). Many of the ideas have been taken from another dwl status bar called [dwl-bar](https://github.com/MadcowOG/dwl-bar). Unlike dwl and dwm, taobar is not configured with a config.h file. Instead, taobar has quite a few command line options which can be used to change how taobar looks. You can find them in the man page or you could also just look at the source code if you want to.

## Dependencies

* [fcft](https://codeberg.org/dnkl/fcft)
* pixman
* libwayland-client
