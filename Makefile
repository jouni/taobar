VERSION = 1.2.1

PKG_CONFIG ?= pkg-config

PREFIX ?= /usr/local
MANPREFIX = $(PREFIX)/share/man

SRC = taobar.c
OBJ = ${SRC:.c=.o}

all: taobar

clean:
	rm -f taobar ${OBJ} *-protocol.*

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f taobar ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/taobar
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "1 s/$$/ $(VERSION)/" < taobar.1 > $(DESTDIR)$(MANPREFIX)/man1/taobar.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/taobar.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/taobar $(DESTDIR)$(MANPREFIX)/man1/taobar.1

WAYLAND_PROTOCOLS=`$(PKG_CONFIG) --variable=pkgdatadir wayland-protocols`
WAYLAND_SCANNER=`$(PKG_CONFIG) --variable=wayland_scanner wayland-scanner`

xdg-shell-protocol.c:
	$(WAYLAND_SCANNER) private-code \
		$(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

wlr-layer-shell-unstable-v1-protocol.h:
	$(WAYLAND_SCANNER) client-header \
		protocols/wlr-layer-shell-unstable-v1.xml $@
wlr-layer-shell-unstable-v1-protocol.c:
	$(WAYLAND_SCANNER) private-code \
		protocols/wlr-layer-shell-unstable-v1.xml $@

xdg-output-unstable-v1-protocol.h:
	$(WAYLAND_SCANNER) client-header \
		$(WAYLAND_PROTOCOLS)/unstable/xdg-output/xdg-output-unstable-v1.xml $@
xdg-output-unstable-v1-protocol.c:
	$(WAYLAND_SCANNER) private-code \
		$(WAYLAND_PROTOCOLS)/unstable/xdg-output/xdg-output-unstable-v1.xml $@

BARCFLAGS = -DVERSION=\"$(VERSION)\" -D_XOPEN_SOURCE=500 \
	     `$(PKG_CONFIG) --cflags pixman-1` $(CFLAGS) -pedantic -Wall -Wextra -Wno-unused-parameter
BARLIBS = `$(PKG_CONFIG) --libs wayland-client fcft pixman-1`

taobar: ${OBJ} wlr-layer-shell-unstable-v1-protocol.o xdg-output-unstable-v1-protocol.o xdg-shell-protocol.o
	$(CC) -o $@ $^ $(BARLIBS) $(LDFLAGS)
taobar.o: utf8.h wlr-layer-shell-unstable-v1-protocol.h xdg-output-unstable-v1-protocol.h
.c.o:
	$(CC) -c $(BARCFLAGS) $<

.PHONY: all clean install
