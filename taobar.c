#include <poll.h>
#include <ctype.h>
#include <errno.h>
#include <fcft/fcft.h>
#include <fcntl.h>
#include <pixman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include "utf8.h"
#include "wlr-layer-shell-unstable-v1-protocol.h"
#include "xdg-output-unstable-v1-protocol.h"

#define USAGE "Usage: taobar [-bv] [-BLFOAU color] [-f fontstr] [-h height] [-t tags] [-s path]"

/* Includes the newline character */
#define MAX_LINE_LEN 8192

struct Monitor {
	char selected;
	int draw;
	char *title;
	char *layout;
	char *xdg_name;
	uint32_t width;
	uint32_t bufsize;
	uint32_t stride;
	int configured;
	struct zxdg_output_v1 *xdg_output;
	uint32_t wl_name;
	struct wl_output *wl_output;
	struct wl_surface *wl_surface;
	struct zwlr_layer_surface_v1 *layer_surface;
	struct wl_list link;
	int otags;
	int atags;
	int utags;
};
static struct wl_list monitors;
static struct zxdg_output_manager_v1 *output_manager;
static uint32_t anchor = ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP;
struct wl_registry *wl_registry;
static int fifo_fd;
static char *fifo_path = NULL;
static int tags = 9;
static char *status = "";

static struct wl_display *display;
static struct wl_compositor *compositor;
static struct wl_shm *shm;
static struct zwlr_layer_shell_v1 *layer_shell;

static uint32_t height;
static int run_display = 0;

static struct fcft_font *font;
static char line[MAX_LINE_LEN];
static int linerem;
static bool eat_line = false;
static pixman_color_t
	bgcolor = {
		.red = 0x3030,
		.green = 0x1d1d,
		.blue = 0x2626,
		.alpha = 0xffff
	},
	bglcolor = {
		.red = 0x5454,
		.green = 0x3333,
		.blue = 0x4444,
		.alpha = 0xffff,
	},
	fgcolor = {
		.red = 0xeaea,
		.green = 0xeaea,
		.blue = 0xeaea,
		.alpha = 0xffff,
	},
	acolor = {
		.red = 0x0000,
		.green = 0xffff,
		.blue = 0x0000,
		.alpha = 0xffff
	},
	ucolor = {
		.red = 0xffff,
		.green = 0x0000,
		.blue = 0x0000,
		.alpha = 0xffff
	},
	ocolor = {
		.red = 0xffff,
		.green = 0x9999,
		.blue = 0x0000,
		.alpha = 0xffff
	};

void
die(char *x)
{
	fprintf(stderr, "Error: %s\n", x);
	exit(EXIT_FAILURE);
}

static void
wl_buffer_release(void *data, struct wl_buffer *wl_buffer)
{
	/* Sent by the compositor when it's no longer using this buffer */
	wl_buffer_destroy(wl_buffer);
}

static const struct wl_buffer_listener wl_buffer_listener = {
	.release = wl_buffer_release,
};

static int
allocate_shm_file(size_t size)
{
	int fd;
	char name[] = "/taobar";
	if ((fd = shm_open(name, O_CREAT | O_RDWR | O_EXCL, 0600)) < 0)
		return -1;
	shm_unlink(name);
	if (ftruncate(fd, size) < 0)
		return -1;
	return fd;
}

/* Color parsing logic adapted from [sway] */
static int
parse_color(const char *str, pixman_color_t *clr)
{
	if (*str == '#')
		str++;
	int len = strlen(str);

	// Disallows "0x" prefix that strtoul would ignore
	if ((len != 6 && len != 8) || !isxdigit(str[0]) || !isxdigit(str[1]))
		return 1;

	char *ptr;
	uint32_t parsed = strtoul(str, &ptr, 16);
	if (*ptr)
		return 1;

	if (len == 8) {
		clr->alpha = (parsed & 0xff) * 0x101;
		parsed >>= 8;
	} else
		clr->alpha = 0xffff;
	clr->red =   ((parsed >> 16) & 0xff) * 0x101;
	clr->green = ((parsed >>  8) & 0xff) * 0x101;
	clr->blue =  ((parsed >>  0) & 0xff) * 0x101;
	return 0;
}

static char *
handle_cmd(char *cmd, pixman_color_t *fg)
{
	char *arg, *end;

	if (!(arg = strchr(cmd, '(')) || !(end = strchr(arg + 1, ')')))
		return cmd;

	*arg++ = '\0';
	*end = '\0';

	if (!strcmp(cmd, "fg")) {
		if (!*arg)
			*fg = fgcolor;
		else if (parse_color(arg, fg))
			fprintf(stderr, "Bad color string \"%s\"\n", arg);
	} else
		fprintf(stderr, "Unrecognized command \"%s\"\n", cmd);

	/* Restore string for later redraws */
	*--arg = '(';
	*end = ')';
	return end;
}

uint32_t
draw_text(char *text, pixman_color_t fg, uint32_t x, uint32_t end, pixman_image_t *image)
{
	if (x >= end)
		return x;
	pixman_image_t *fgfill = pixman_image_create_solid_fill(&fg);

	uint32_t xpos = x, ypos = (height + font->ascent - font->descent) / 2, maxxpos = x;
	uint32_t codepoint, lastcp = 0, state = UTF8_ACCEPT;
	for (char *p = text; *p; p++) {
		/* Check for inline ^ commands */
		if (state == UTF8_ACCEPT && *p == '^') {
			p++;
			if (*p != '^') {
				p = handle_cmd(p, &fg);
				pixman_image_unref(fgfill);
				fgfill = pixman_image_create_solid_fill(&fg);
				continue;
			}
		}

		/* Returns nonzero if more bytes are needed */
		if (utf8decode(&state, &codepoint, *p))
			continue;

		/* Turn off subpixel rendering, which complicates things when
		 * mixed with alpha channels */
		const struct fcft_glyph *glyph = fcft_rasterize_char_utf32(font, codepoint,
				FCFT_SUBPIXEL_NONE);
		if (!glyph)
			continue;

		/* Adjust x position based on kerning with previous glyph */
		long x_kern = 0;
		if (lastcp)
			fcft_kerning(font, lastcp, codepoint, &x_kern, NULL);
		xpos += x_kern;
		lastcp = codepoint;

		if (image) {
			/* Detect and handle pre-rendered glyphs (e.g. emoji) */
			if (pixman_image_get_format(glyph->pix) == PIXMAN_a8r8g8b8)
				/* Only the alpha channel of the mask is used, so we can
				 * use fgfill here to blend prerendered glyphs with the
				 * same opacity */
				pixman_image_composite32(
					PIXMAN_OP_OVER, glyph->pix, fgfill, image, 0, 0, 0, 0,
					xpos + glyph->x, ypos - glyph->y, glyph->width, glyph->height);
			else
				/* Applying the foreground color here would mess up
				 * component alphas for subpixel-rendered text, so we
				 * apply it when blending. */
				pixman_image_composite32(
					PIXMAN_OP_OVER, fgfill, glyph->pix, image, 0, 0, 0, 0,
					xpos + glyph->x, ypos - glyph->y, glyph->width, glyph->height);
		}

		/* increment pen position */
		ypos += glyph->advance.y;
		if (maxxpos < (xpos += glyph->advance.x) && (maxxpos = xpos) >= end)
			break;
	}
	pixman_image_unref(fgfill);
	if (state != UTF8_ACCEPT)
		fprintf(stderr, "malformed UTF-8 sequence\n");
	return maxxpos;
}

void
draw_frame(struct Monitor *monitor)
{
	uint32_t wl = 0, wr = 0;
	pixman_color_t color;
	int fd, i = 0;

	/* Allocate buffer to be attached to the surface */
	if ((fd = allocate_shm_file(monitor->bufsize)) == -1)
		return;

	uint32_t *data = mmap(NULL, monitor->bufsize,
			PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (data == MAP_FAILED) {
		close(fd);
		return;
	}

	struct wl_shm_pool *pool = wl_shm_create_pool(shm, fd, monitor->bufsize);
	struct wl_buffer *buffer = wl_shm_pool_create_buffer(pool, 0,
			monitor->width, height, monitor->stride, WL_SHM_FORMAT_ARGB8888);
	wl_buffer_add_listener(buffer, &wl_buffer_listener, NULL);
	wl_shm_pool_destroy(pool);
	close(fd);

	/* Pixman image corresponding to main buffer */
	pixman_image_t *bar = pixman_image_create_bits(PIXMAN_a8r8g8b8,
			monitor->width, height, data, monitor->width * 4);
	pixman_image_t *foreground = pixman_image_create_bits(PIXMAN_a8r8g8b8,
			monitor->width, height, NULL, monitor->width * 4);
	/* Fill bar with background color if bar should extend beyond text */
	pixman_image_fill_boxes(PIXMAN_OP_SRC, bar, &bgcolor, 1,
			&(pixman_box32_t) {.x1 = 0, .x2 = monitor->width, .y1 = 0, .y2 = height});

	wr = monitor->width - draw_text(status, fgcolor, 0, monitor->width, NULL);
	do {
		char buf[3] = "";
		color = (monitor->atags & 1 << i) ? acolor :
			(monitor->utags & 1 << i) ? ucolor :
			(monitor->otags & 1 << i) ? ocolor :
			fgcolor;
		snprintf(buf, 3, "%i", i + 1);
		wl = draw_text(buf, color, wl, wr, foreground) + 3;
	} while (++i < tags && i < 31);
	pixman_image_fill_boxes(PIXMAN_OP_SRC, bar, &bglcolor, 1,
			&(pixman_box32_t) {.x1 = 0, .x2 = wl, .y1 = 0, .y2 = height});
	if (monitor->layout) {
		wl += 3;
		wl = draw_text(monitor->layout, fgcolor, wl, wr, foreground) + 3;
	}
	if (monitor->selected == '1') {
		pixman_image_fill_boxes(PIXMAN_OP_SRC, bar, &bglcolor, 1,
				&(pixman_box32_t) {.x1 = wl, .x2 = (wr < monitor->width) ? wr - 3 : wr, .y1 = 0, .y2 = height});
		if (monitor->title)
			draw_text(monitor->title, fgcolor, wl + 3, wr - 3, foreground);
		draw_text(status, fgcolor, wr, monitor->width, foreground);
	}

	pixman_image_composite32(PIXMAN_OP_OVER, bar, NULL, bar, 0, 0, 0, 0,
			0, 0, monitor->width, height);
	pixman_image_composite32(PIXMAN_OP_OVER, foreground, NULL, bar, 0, 0, 0, 0,
			0, 0, monitor->width, height);
	pixman_image_unref(foreground);
	pixman_image_unref(bar);
	munmap(data, monitor->bufsize);

	wl_surface_attach(monitor->wl_surface, buffer, 0, 0);
	wl_surface_damage_buffer(monitor->wl_surface, 0, 0, monitor->width, height);
	wl_surface_commit(monitor->wl_surface);
}

/* Layer-surface setup adapted from layer-shell example in [wlroots] */
static void
layer_surface_configure(void *data,
		struct zwlr_layer_surface_v1 *surface,
		uint32_t serial, uint32_t w, uint32_t h)
{
	struct Monitor *monitor = data;
	zwlr_layer_surface_v1_ack_configure(surface, serial);
	if (!monitor->configured) {
		monitor->width = w;
		monitor->stride = w * 4;
		monitor->bufsize = monitor->stride * h;
		monitor->configured = 1;
		draw_frame(monitor);
	}
}

static void
layer_surface_closed(void *data, struct zwlr_layer_surface_v1 *surface)
{
	struct Monitor *monitor = data;
	zwlr_layer_surface_v1_destroy(surface);
	wl_surface_destroy(monitor->wl_surface);
}

static struct zwlr_layer_surface_v1_listener layer_surface_listener = {
	.configure = layer_surface_configure,
	.closed = layer_surface_closed,
};

void
xdg_output_name(void *data, struct zxdg_output_v1 *output, const char *name)
{
	struct Monitor *monitor = data;
	monitor->xdg_name = strdup(name);
	zxdg_output_v1_destroy(output);
}

static const struct zxdg_output_v1_listener xdg_output_listener = {
	.name = xdg_output_name,
};

void
monitor_initialize(struct Monitor *monitor)
{
	monitor->xdg_output = zxdg_output_manager_v1_get_xdg_output(output_manager, monitor->wl_output);
	zxdg_output_v1_add_listener(monitor->xdg_output, &xdg_output_listener, monitor);

	/* Create layer-shell surface */
	monitor->wl_surface = wl_compositor_create_surface(compositor);
	if (!monitor->wl_surface)
		die("could not create wl_surface");

	monitor->layer_surface = zwlr_layer_shell_v1_get_layer_surface(layer_shell,
			monitor->wl_surface, monitor->wl_output, ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM, "taobar");
	if (!monitor->layer_surface)
		die("could not create layer_surface");
	zwlr_layer_surface_v1_add_listener(monitor->layer_surface,
			&layer_surface_listener, monitor);

	/* Set layer size and positioning */
	zwlr_layer_surface_v1_set_size(monitor->layer_surface, monitor->width, height);
	zwlr_layer_surface_v1_set_anchor(monitor->layer_surface,
			ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT | anchor);
	zwlr_layer_surface_v1_set_exclusive_zone(monitor->layer_surface, height);

	wl_surface_commit(monitor->wl_surface);
}

void
monitor_destroy(struct Monitor *monitor)
{
	if (!monitor)
		return;
	free(monitor->xdg_name);
	free(monitor->title);
	free(monitor->layout);
	if (wl_output_get_version(monitor->wl_output) >= WL_OUTPUT_RELEASE_SINCE_VERSION)
		wl_output_release(monitor->wl_output);
	else
		wl_output_destroy(monitor->wl_output);
	free(monitor);
}

static void
handle_global(void *data, struct wl_registry *registry,
		uint32_t name, const char *interface, uint32_t version)
{
	if (!strcmp(interface, wl_compositor_interface.name))
		compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 4);
	else if (!strcmp(interface, wl_shm_interface.name))
		shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
	else if (!strcmp(interface, zwlr_layer_shell_v1_interface.name))
		layer_shell = wl_registry_bind(registry, name, &zwlr_layer_shell_v1_interface, 1);
	else if (!strcmp(interface, zxdg_output_manager_v1_interface.name))
		output_manager = wl_registry_bind(registry, name, &zxdg_output_manager_v1_interface, 3);
	else if (!strcmp(interface, wl_output_interface.name)) {
		struct Monitor *monitor;
		if ((monitor = calloc(1, sizeof(*monitor)))) {
			monitor->selected = '0';
			monitor->draw = 0;
			monitor->title = NULL;
			monitor->layout = NULL;
			monitor->wl_output = wl_registry_bind(registry, name, &wl_output_interface, 1);
			monitor->wl_name = name;
			wl_list_insert(&monitors, &monitor->link);
			if (run_display)
				monitor_initialize(monitor);
		}
	}
}

void
handle_global_remove(void *data, struct wl_registry *registry, uint32_t name)
{
	struct Monitor *monitor, *tmp_monitor;
	wl_list_for_each_safe(monitor, tmp_monitor, &monitors, link) {
		if (monitor->wl_name != name)
			continue;
		wl_list_remove(&monitor->link);
		monitor_destroy(monitor);
	}
}

static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = handle_global_remove,
};

static void
read_stdin(void)
{
	struct Monitor *monitor;
	char *token;
	ssize_t b;

	/* Read as much data as we can into line buffer */
	if ((b = read(STDIN_FILENO, line + linerem, MAX_LINE_LEN - linerem)) <= 0) {
		run_display = 0;
		return;
	}
	linerem += b;

	/* Handle each line in the buffer in turn */
	char *curline, *end;
	for (curline = line; (end = memchr(curline, '\n', linerem)); curline = end) {
		struct Monitor *m = NULL;

		*end++ = '\0';
		linerem -= end - curline;

		if (eat_line) {
			eat_line = false;
			continue;
		}

		if (!(token = strtok(curline, " ")))
			continue;

		wl_list_for_each(monitor, &monitors, link)
			if (monitor->xdg_name && !strcmp(monitor->xdg_name, token)) {
				m = monitor;
				break;
			}
		if (!m)
			continue;

		if (!(token = strtok(NULL, " ")))
			continue;
		if (!strcmp("title", token)) {
			free(m->title);
			m->title = (token = strtok(NULL, "\n")) ? strdup(token) : NULL;
			m->draw = 1;
		} else if (!strcmp("layout", token)) {
			free(m->layout);
			m->layout = (token = strtok(NULL, "\n")) ? strdup(token) : NULL;
			m->draw = 1;
		} else if (!strcmp("selmon", token)) {
			if ((token = strtok(NULL, " ")) && token[0] != m->selected) {
				m->selected = token[0];
				m->draw = 1;
			}
		} else if (!strcmp("tags", token)) {
			if (!(token = strtok(NULL, " ")))
				continue;
			m->otags = atoi(token);
			if (!(token = strtok(NULL, " ")))
				continue;
			m->atags = atoi(token);
			if (!strtok(NULL, " "))
				continue;
			//skip
			if (!(token = strtok(NULL, " ")))
				continue;
			m->utags = atoi(token);
			m->draw = 1;
		}
	}

	if (linerem == MAX_LINE_LEN || eat_line) {
		/* Buffer is full, so discard current line */
		linerem = 0;
		eat_line = true;
	} else if (linerem && curline != line)
		/* Shift any remaining data over */
		memmove(line, curline, linerem);
}

static void
event_loop(void)
{
	int ret;
	int wlfd = wl_display_get_fd(display);
	struct Monitor *monitor;
	char *first, *x;
	struct pollfd fds[] = {
		{.fd = STDIN_FILENO, .events = POLLIN},
		{.fd = fifo_fd, .events = POLLIN},
		{.fd = wlfd, .events = POLLIN}
	};

	while (run_display) {
		/* Does this need to be inside the loop? */
		wl_display_flush(display);

		if ((ret = poll(fds, 3, -1)) < 0) {
			fprintf(stderr, "poll\n");
			break;
		}

		if (fds[0].revents & POLLIN)
			read_stdin();

		if (fds[1].revents & POLLIN) {
			char line[MAX_LINE_LEN] = "";
			if ((ret = read(fifo_fd, line, MAX_LINE_LEN)) < 0) {
				fprintf(stderr, "read\n");
				break;
			}
			if (ret > 0) {
				first = (first = strtok(line, "\n")) ? first : line;
				status = "";
				for (x = first; *x != '\0'; ++x) {
					if (isspace(*x))
						continue;
					status = first;
					break;
				}
				wl_list_for_each(monitor, &monitors, link)
					monitor->draw = 1;
			}
		}

		if (fds[2].revents & POLLIN)
			if (wl_display_dispatch(display) == -1)
				break;

		wl_list_for_each(monitor, &monitors, link)
			if (monitor->draw) {
				draw_frame(monitor);
				monitor->draw = 0;
			}
	}
}

void
termination_handler()
{
	struct Monitor *monitor, *tmp_monitor;
	wl_list_for_each_safe(monitor, tmp_monitor, &monitors, link) {
		wl_list_remove(&monitor->link);
		monitor_destroy(monitor);
	}
	close(fifo_fd);
	unlink(fifo_path);
	free(fifo_path);
	fcft_destroy(font);
	fcft_fini();
	zxdg_output_manager_v1_destroy(output_manager);
	zwlr_layer_shell_v1_destroy(layer_shell);
	wl_shm_destroy(shm);
	wl_compositor_destroy(compositor);
	wl_registry_destroy(wl_registry);
	wl_display_disconnect(display);
}

void
signal_handler(int signal)
{
	struct Monitor *monitor;
	wl_list_for_each(monitor, &monitors, link) {
		zwlr_layer_surface_v1_destroy(monitor->layer_surface);
		wl_surface_destroy(monitor->wl_surface);
	}
	termination_handler();
	exit(signal);
}

#define CERR(x) do { fprintf(stderr, "malformed color string for -%c\n", x); return 1; } while(0)

int
main(int argc, char **argv)
{
	char *fontstr = "";
	struct Monitor *monitor;
	struct sigaction saction;
	int i, len;
	unsigned h = 0;
	char *runtime_path;

	while ((i = getopt(argc, argv, "s:B:F:O:A:U:L:f:h:t:vb")) != -1)
		switch (i) {
			case 'v':
				printf("%s\n", VERSION);
				return 0;
			case 'B':
				if (parse_color(optarg, &bgcolor))
					CERR('B');
				break;
			case 'F':
				if (parse_color(optarg, &fgcolor))
					CERR('F');
				break;
			case 'O':
				if (parse_color(optarg, &ocolor))
					CERR('O');
				break;
			case 'A':
				if (parse_color(optarg, &acolor))
					CERR('A');
				break;
			case 'U':
				if (parse_color(optarg, &ucolor))
					CERR('U');
				break;
			case 'L':
				if (parse_color(optarg, &bglcolor))
					CERR('L');
				break;
			case 'f':
				fontstr = optarg;
				break;
			case 'h':
				if ((i = atoi(optarg)) > 0)
					h = i;
				break;
			case 'b':
				anchor = ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM;
				break;
			case 't':
				tags = atoi(optarg);
				break;
			case 's':
				fifo_path = strdup(optarg);
				break;
			default:
				printf("%s\n", USAGE);
				return 0;
		}

	/* Set up display and protocols */
	if (!(display = wl_display_connect(NULL))) {
		fprintf(stderr, "Failed to create display\n");
		return 1;
	}

	/* Load selected font */
	fcft_init(FCFT_LOG_COLORIZE_AUTO, 0, FCFT_LOG_CLASS_ERROR);
	fcft_set_scaling_filter(FCFT_SCALING_FILTER_LANCZOS3);
	if (!(font = fcft_from_name(1, (const char *[]) {fontstr}, NULL)))
		die("could not load font");
	height = font->ascent + font->descent;
	if (h) {
		if (height > h)
			fprintf(stderr, "Height is too short\n");
		else
			height = h;
	}

	wl_list_init(&monitors);

	wl_registry = wl_display_get_registry(display);
	wl_registry_add_listener(wl_registry, &registry_listener, NULL);
	wl_display_roundtrip(display);

	if (!compositor || !shm || !layer_shell)
		die("compositor does not support all needed protocols");
	wl_list_for_each(monitor, &monitors, link)
		monitor_initialize(monitor);

	/* Set up the structure to specify the new action. */
	saction.sa_handler = signal_handler;
	sigemptyset(&saction.sa_mask);
	saction.sa_flags = 0;

	sigaction(SIGTERM, &saction, NULL);
	sigaction(SIGINT, &saction, NULL);
	sigaction(SIGCHLD, &saction, NULL);

	if (!fifo_path) {
		runtime_path = getenv("XDG_RUNTIME_DIR");
		len = strlen(runtime_path) + 11;
		if ((fifo_path = malloc(len * sizeof(char))))
			for (i = 0; i < 100; i++) {
				snprintf(fifo_path, len, "%s/taobar-%d", runtime_path, i);
				if (mkfifo(fifo_path, 0666) == 0)
					break;
				if (errno != EEXIST)
					die("failed to create fifo");
			}
		else
			die("out of memory");
	} else if (mkfifo(fifo_path, 0666) < 0)
		die("failed to create fifo");
	if ((fifo_fd = open(fifo_path, O_RDWR)) < 0)
		die("failed to open fifo");

	run_display = 1;
	event_loop();
	termination_handler();
	return 0;
}
